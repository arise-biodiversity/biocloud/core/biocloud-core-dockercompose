# Docker compose

This repo contains the base docker-compose file that contains services that are shared by biocloud-core and other repos. This container should always be running.


## The docker-compose setup


### Running all services

Run all services:
```bash
docker compose up -d
```

Check if everything is up and running:
```bash
docker compose ps
docker compose logs --follow # Follow the logs from all services
```

Shut down all services:
```bash
docker compose down
```

And for serious cleaning:
```bash
docker system prune --all
```
This will remove all unused containers, networks, images (both dangling and unreferenced), and optionally, volumes.



### Running individual services:

For development purposes, it may be convenient to start for instance only the spark cluster:

```bash
docker compose up -d spark spark-worker
```

If you have one or more services running, you can easily include others:

```bash
docker compose up -d biocloud-core
```

To get an overview of all services that are running (or have stopped):
```bash
docker compose ps
```

Finally, if you'd like to stop just one service and leave the rest running:

```bash
docker compose stop biocloud-core
```

### MinIO

For development purposes it may be useful to add MinIO to your (local) environment. When you need MinIO just copy the `docker-compose.override.yml.dev` file to `docker-compose.override.yml`. When you next run your docker compose setup, a MinIO server will be included in the mix.

MinIO is available at http://localhost:9000 or http://127.0.0.1:9000. However, when you're running an application outside of the docker network you need to make sure that your code and the spark cluster share the same MinIO endpoint. You can use the host from the docker bridge network for that. Most docker engine installations will represent the host as `172.17.0.1` on the default `docker0 bridge network`. You can check yours by running: `ip addr show docker0`. Use that address for instance as value for `S3_ENDPOINT`. 


## Using the services

- Spark Web UI: [https://localhost/](https://localhost/)
- Retrieve faunabit metadata: [https://localhost/faunabit](https://localhost/faunabit)
- Generate image metadata and ferch payload: [https://localhost/image_data](https://localhost/image_data)
- Create delta tables: [https://localhost/biocloud-core](https://localhost/biocloud-core)
- MinIO: [http://localhost:9001/login](http://localhost:9001/login)

NOTE: because traefik has bee configured to use certicates, your browser will probably complain when running the service locally. In that case, ignore te warning and continue using `localhost`.